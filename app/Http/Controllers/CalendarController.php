<?php
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use Microsoft\Graph\Event;
use App\TokenStore\TokenCache;


class CalendarController extends Controller
{
  public function calendar()
  {
    $viewData = $this->loadViewData();

    // Get the access token from the cache
    $tokenCache = new TokenCache();
    $accessToken = $tokenCache->getAccessToken();

    // Create a Graph client
    $graph = new Graph();
    $graph->setAccessToken($accessToken);

    $queryParams = array(
      '$select' => 'subject,organizer,start,end',
      '$orderby' => 'createdDateTime DESC'
    );

    // Append query parameters to the '/me/events' url
    $getEventsUrl = '/me/events?'.http_build_query($queryParams);

    $events = $graph->createRequest('GET', $getEventsUrl)
      ->setReturnType(Model\Event::class)
      ->execute();

    $viewData['events'] = $events;
    return view('calendar', $viewData);
  }

  public function create(){
      $viewData = $this->loadViewData();

      // Get the access token from the cache
      $tokenCache = new TokenCache();
      $accessToken = $tokenCache->getAccessToken();

      // Create a Graph client
      $graph = new Graph();
      $graph->setAccessToken($accessToken);

      return view('create',$viewData);
  }

    public function store(Request $request){
        $viewData = $this->loadViewData();

        // Get the access token from the cache
        $tokenCache = new TokenCache();
        $accessToken = $tokenCache->getAccessToken();

        // Create a Graph client
        $graph = new Graph();
        $graph->setAccessToken($accessToken);

        $post = [
            "Subject" => $request->subject,
            "Start" => [
                "DateTime" => $request->dateStart,
                "TimeZone" => $request->timeZone
            ],
            "End" => [
                "DateTime" => $request->dateEnd,
                "TimeZone" => $request->timeZone
            ],
        ];

        $graph->createRequest("POST","/me/calendar/events")->attachBody($post)->setReturnType(Model\Event::class)->
        addHeaders(array("Content-Type"=>"application/json"))->execute();

        return redirect('/calendar');
    }
}
