<!-- Copyright (c) Microsoft Corporation.
     Licensed under the MIT License. -->

<!-- <WelcomeSnippet> -->
@extends('layout')

@section('content')
<div class="jumbotron">
  <h1>Create Calendar event</h1>
  <p class="lead">In this page you will include calendar details to add to your event</p>

    <h4>Welcome {{ $userName }}!</h4>

    <form action="/storeEvent" method="POST">
        <div>Event name</div>
        <input name="subject">
        <div>Event starts</div>
        <input type="datetime-local" name="dateStart">
        <input hidden type="string" value="Eastern Standard Time" name="timeZone">
        <div>Event ends</div>
        <input type="datetime-local" name="dateEnd">

        <button type="submit">
        @csrf

    </form>


</div>
@endsection
<!-- </WelcomeSnippet> -->
